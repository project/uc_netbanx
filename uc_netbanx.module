<?php
/**
 * @file
 * Integrates netbanx's redirected payment service with Ubercart.
 *
 */

/**
 * Implementation of hook_menu().
 */
function uc_netbanx_menu() {
  
  $items['cart/netbanx/success'] = array(
    'title' => t('Order complete'),
    'page callback' => 'uc_netbanx_success',
    'access arguments' => array('access content'),
    'type' => MENU_CALLBACK,
  );

  $items['cart/netbanx/failure'] = array(
    'title' => t('Order failed'),
    'page callback' => 'uc_netbanx_failure',
    'access arguments' => array('access content'),
    'type' => MENU_CALLBACK,
  );
  
  $items['cart/netbanx/return/%/%'] = array(
    'title' => t('Order returned - testing'),
    'page callback' => 'uc_netbanx_return',
    'page arguments' => array(3, 4),
    'access arguments' => array('access content'),
    'type' => MENU_CALLBACK,
  );

  return $items;

}

/**
 * Implementation of hook_payment_method().
 */
function uc_netbanx_payment_method() {  
  $methods[] = array(
    'id' => 'netbanx',
    'name' => t('netbanx'),
    'title' => t('Credit Card (via NETBANX)'),
    'desc' => t('Redirect to netbanx to pay by credit card or eCheck.'),
    'callback' => 'uc_payment_method_netbanx',
    'weight' => 3,
    'checkout' => TRUE,
    'no_gateway' => TRUE,
  );

  return $methods;
}


/**
 * Payment method settings.
 */
function uc_payment_method_netbanx($op, &$arg1) {
  switch ($op) {

    case 'cart-process':
      $_SESSION['pay_method'] = $_POST['pay_method'];
      return;

    case 'settings':
      $form['help_text']['netbanx_settings'] = array(
        '#type' => 'item',
        '#prefix' => '<div class="help">',
        '#value' => t("<h4><strong>Ubercart NetBanx Installation Instructions:</strong></h4><p>Please edit the values below, with reference to the netbanx documentations</p>"),
        '#suffix' => '</div>',
      );

      $form['uc_netbanx_url'] = array(
        '#type' => 'textfield',
        '#title' => t('Netbanx merchant transaction URL'),
        '#default_value' => variable_get('uc_netbanx_url', 'http://pay.netbanx.com/YOUR_MERCHANT_NAME_HERE'),
        '#size' => 256,
      );

      $form['uc_netbanx_checkout_button'] = array(
        '#type' => 'textfield',
        '#title' => t('Order review submit button text'),
        '#description' => t('Alter the text of the submit button on the review order page.'),
        '#default_value' => variable_get('uc_netbanx_checkout_button', t('Submit Order')),
      );

      $netbanx_payment_types = array(
        'card' => t('Credit/Debit Card'),
        'directpay24' => t('DirectPay24'),
        'neteller' => t('NETELLER wallet'),
        'ukash' => t('Ukash'),
        'paypal' => t('PayPal'), 
        'poli' => t('POLI'),
      );

      $form['uc_netbanx_payment_methods'] = array(
        '#title' => t('Payment types'),
        '#description' => t('Select the payment methods to display in checkout.'),
        '#type' => 'checkboxes',
        '#default_value' => variable_get('uc_netbanx_payment_methods', array_keys($netbanx_payment_types)),
        '#options' => $netbanx_payment_types,
      );

      $form['uc_netbanx_return_url'] = array(
        '#type' => 'textfield',
        '#title' => t('Return URL'),
        '#description' => t('The URL your user will be taken to after returning from the netbanx site.'),
        '#default_value' => variable_get('uc_netbanx_return_url', t('/')),
      );
      
      $form['uc_netbanx_currency_factor'] = array(
        '#type' => 'textfield',
        '#title' => t('Minor currency unit multiplier'),
        '#description' => t('Netbanx uses integer minor currency units, Ubercart uses decimal major currency units. This is the the multiplication factor to use to convert between them (usually 100).'),
        '#default_value' => variable_get('uc_netbanx_currency_factor', 100),
      );

      $form['uc_netbanx_secret_key'] = array(
        '#type' => 'textfield',
        '#title' => t('Secret Key'),
        '#description' => t('Your chosen secret key that will be used to create the checksum for transmitted data.'),
        '#default_value' => variable_get('uc_netbanx_secret_key', t('SECRETKEY')),
      );
   
      return $form;
  }
}

/**
 * Implementation of hook_form_alter().
 *
 * Inserts netbanx fields into form
 */
function uc_netbanx_form_alter(&$form, $form_state, $form_id) {
  
  if ($form_id == 'uc_cart_checkout_review_form' && ($order_id = intval($_SESSION['cart_order'])) > 0) {
  
    $order = uc_order_load($order_id);

    if ($order->payment_method == 'netbanx') {
  
      $checksum = _nbx_checksum_from_order($order);
  
      $netbanx_fields = array(
        'uc_netbanx_order_id' => $order->order_id,
        'nbx_payment_amount' => round($order->order_total * variable_get('uc_netbanx_currency_factor', 100)),
        'nbx_currency_code' => 'GBP',
        'nbx_merchant_reference' => $order->order_id,
        'nbx_checksum' => $checksum,
        'nbx_language' => variable_get('uc_netbanx_language', 'en'),
        'nbx_success_url' => 'http://'. $_SERVER['HTTP_HOST'] . '/cart/netbanx/success',
        'nbx_failure_url' => 'http://'. $_SERVER['HTTP_HOST'] . '/cart/netbanx/failure',  
        'nbx_return_url' => 'http://'. $_SERVER['HTTP_HOST'] . '/cart/netbanx/return/' . $order->order_id . '/' . $checksum,
        'nbx_email' => $order->primary_email,
        'nbx_cardholder_name' => $order->billing_first_name .' '. $order->billing_last_name,
        'nbx_houseno' => $order->billing_street1,
        'nbx_postcode' => $order->billing_postal_code,
        'nbx_payment_type' => join(',', array_keys(variable_get('uc_netbanx_payment_methods', '')))
      );
      
      foreach ($netbanx_fields as $name => $value) {
        $form[$name] = array(
          '#type' => 'hidden',
          '#value' => $value,
        );
      }

      $form['#action'] = variable_get('uc_netbanx_url', '');
      $form['submit'] = array(
        '#type' => 'submit',
        '#name' => '',
        '#value' => variable_get('uc_netbanx_checkout_button', t('Submit Order')),
      );
    }
  }
}

/**
 * Called by netbanx CGI success callback
 *
 * @return void
 * @author Andrew Larcombe
 */
function uc_netbanx_success() {
  
  $nbx_status = $_POST['nbx_status'];
  $uc_netbanx_order_id = $_POST['uc_netbanx_order_id'];
  $nbx_netbanx_reference = $_POST['nbx_netbanx_reference'];
  $nbx_payment_amount = $_POST['nbx_payment_amount'];
  $all_post_params = print_r($_POST, 1);
  
  $message = t('Netbanx CGI success callback called with parameters: <pre>!all_post_params</pre>', array('!all_post_params' => $all_post_params));
  watchdog('uc_netbanx', $message);
  
  //NB THE NETBANX DOCS ARE INCORRECT - NBX_STATUS is 'passed'
  if ($nbx_status != 'passed') {
    watchdog('uc_netbanx', t('Success URL called, but nbx_status not set to authorised.'));
    exit();
  }
  
  if (!($order = uc_order_load($uc_netbanx_order_id))) {
    watchdog('uc_netbanx', t('The order !uc_netbanx_order_id could not be found', array('!uc_netbanx_order_id' => $uc_netbanx_order_id)));
    exit();
  }

  $comment = t('Paid £!nbx_payment_amount, netbanx reference !nbx_netbanx_reference.', array('!nbx_payment_amount' => $nbx_payment_amount , '!nbx_netbanx_reference' => $nbx_netbanx_reference));
  uc_payment_enter($order->order_id, 'netbanx', $nbx_payment_amount / variable_get('uc_netbanx_currency_factor', 100) , 0, "netbanx_reference={$nbx_netbanx_reference}", $comment);

  uc_order_comment_save($order->order_id, 0, t('Order created through website - succesful payment through netbanx'), 'admin');

  if ($order->order_total * variable_get('uc_netbanx_currency_factor', 100) == $nbx_payment_amount) {
    uc_cart_complete_sale($order);
  }
  else {
    uc_order_comment_save($order->order_id, 0, t('Payment from netbanx did not match order total'), 'admin');
  }

  exit();
  
}


/**
 * Called by netbanx CGI failure callback
 *
 * @return void
 * @author Andrew Larcombe
 */
function uc_netbanx_failure() {
  
  $nbx_status = $_POST['nbx_status'];
  $uc_netbanx_order_id = $_POST['uc_netbanx_order_id'];
  $nbx_netbanx_reference = $_POST['nbx_netbanx_reference'];
  $nbx_payment_amount = $_POST['nbx_payment_amount'];
  $all_post_params = print_r($_POST, 1);
  
  $message = t('Netbanx CGI failure callback called with parameters: <pre>!all_post_params</pre>', array('!all_post_params' => $all_post_params));
  watchdog('uc_netbanx', $message);
  
  if (!$order = uc_order_load($uc_netbanx_order_id)) {
    watchdog('uc_netbanx', t('The order !uc_netbanx_order_id could not be found', array('!uc_netbanx_order_id' => $uc_netbanx_order_id)));
    exit();
  }

  switch ($nbx_status) {
    case 'pending':
    case 'declined':
    case 'failure':
      watchdog('uc_netbanx', t('Payment marked as !nbx_status for order !uc_netbanx_order_id', array('!nbx_status' => $nbx_status, '!uc_netbanx_order_id' => $uc_netbanx_order_id)));
      uc_order_comment_save($order->order_id, 0, t('Payment marked as pending by netbanx.'), 'admin');    
      break;
    
    default:
      watchdog('uc_netbanx', t('Unknown failure status !nbx_status for order !uc_netbanx_order_id', array('!nbx_status' => $nbx_status, '!uc_netbanx_order_id' => $uc_netbanx_order_id)));
      uc_order_comment_save($order->order_id, 0, t('Payment marked as pending by netbanx.'), 'admin');    
      break;
  }
  
  uc_order_update_status($order->order_id, 'canceled');
  
  exit();
  
}

/**
 * returns a checksum from an order
 *
 * @param string $order 
 * @return string
 * @author Andrew Larcombe
 */
function _nbx_checksum_from_order($order) {

  return sha1(($order->order_total * variable_get('uc_netbanx_currency_factor', 100)) . 'GBP' . ($order_total->order_id) . variable_get('uc_netbanx_secret_key', 'SECRETKEY'));

}

/**
 * Check we've been passed a valid order_id & checksum, then empty the cart and go to the 'thank you' page
 *
 * @return void
 * @author Andrew Larcombe
 */
function uc_netbanx_return($order_id, $checksum) {
  
  if (!($order = uc_order_load($order_id))) {
    watchdog('uc_netbanx', t('uc_netbanx_return was called with order_id !order_id - failed to load order', array('!order_id' => $order_id)));
    return drupal_not_found();
  }
    
  if ($checksum != _nbx_checksum_from_order($order)) {
    watchdog('uc_netbanx', t('uc_netbanx_return was called with invalid checksum for order_id !order_id', array('!order_id' => $order_id)));
    return drupal_not_found();
  }

  uc_cart_empty(uc_cart_get_id());

  drupal_goto(variable_get('uc_netbanx_return_url', '/'));
  
}
